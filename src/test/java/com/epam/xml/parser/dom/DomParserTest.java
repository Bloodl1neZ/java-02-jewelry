package com.epam.xml.parser.dom;

import com.epam.xml.entity.*;
import com.epam.xml.exception.ParserException;
import com.epam.xml.parser.Parser;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class DomParserTest {
    private static final String INPUT_FILE = "src/test/resources/gems.xml";

    @Test
    public void shouldParseWhenDataValid() throws ParserException {
        //given
        Parser parser = new DomParser();
        //when
        List<Stone> actual = parser.parse(INPUT_FILE);
        //then
        Assert.assertEquals(2, actual.size());
        Stone first = actual.get(0);
        Assert.assertEquals(PreciousGem.class, first.getClass());
        Assert.assertEquals("ruby", first.getName());
        Assert.assertEquals(Country.RUSSIA, first.getOrigin());
        BigDecimal bigDecimalFirstExpected = new BigDecimal(23.5);
        Assert.assertEquals(bigDecimalFirstExpected, first.getValue());
        Assert.assertEquals(Color.RED, first.getColor());
        Assert.assertEquals(CuttingType.ROSE, first.getCuttingType());
        Assert.assertTrue(((PreciousGem)first).isNatural());
        Assert.assertEquals(20, ((PreciousGem)first).getTransparency());

        Stone second = actual.get(1);
        Assert.assertEquals(SemiPreciousGem.class, second.getClass());
        Assert.assertEquals("citrine", second.getName());
        Assert.assertEquals(Country.USA, second.getOrigin());
        BigDecimal bigDecimalSecondExpected = new BigDecimal(35.5);
        Assert.assertEquals(bigDecimalSecondExpected, second.getValue());
        Assert.assertEquals(Color.YELLOW, second.getColor());
        Assert.assertEquals(CuttingType.PEAR, second.getCuttingType());
        Assert.assertEquals(Source.MAGMA, ((SemiPreciousGem)second).getSource());
    }

    @Test (expected = ParserException.class)
    public void shouldParseAndThrowExceptionCauseInvalidPath() throws ParserException {
        //given
        Parser parser = new DomParser();
        //when
        parser.parse("qwe.xml");
    }
}
