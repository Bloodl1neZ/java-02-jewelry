package com.epam.xml.parser;

import com.epam.xml.exception.ParserException;
import com.epam.xml.parser.dom.DomParser;
import com.epam.xml.parser.jaxb.JaxbParser;
import com.epam.xml.parser.sax.SaxParser;
import org.junit.Assert;
import org.junit.Test;

public class ParserFactoryTest {
    private final ParserFactory parserFactory = new ParserFactory();

    @Test
    public void shouldCreateDomParser() throws ParserException {
        //given
        //when
        Parser domParser = parserFactory.createParser(TypeParser.DOM);
        //then
        Assert.assertEquals(DomParser.class, domParser.getClass());
    }
    @Test
    public void shouldCreateSaxParser() throws ParserException {
        //given
        //when
        Parser saxParser = parserFactory.createParser(TypeParser.SAX);
        //then
        Assert.assertEquals(SaxParser.class, saxParser.getClass());
    }
    @Test
    public void shouldCreateJaxbParser() throws ParserException {
        //given
        //when
        Parser jaxbParser = parserFactory.createParser(TypeParser.JAXB);
        //then
        Assert.assertEquals(JaxbParser.class, jaxbParser.getClass());
    }
}
