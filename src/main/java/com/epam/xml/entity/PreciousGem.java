package com.epam.xml.entity;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@XmlSeeAlso({Stone.class})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "precious-gem")
public class PreciousGem extends Stone {

    @XmlElement(name = "natural")
    private boolean natural;

    @XmlElement(name = "transparency")
    private short transparency;

    public PreciousGem(String name, Country origin, BigDecimal value, Color color,
                       CuttingType cuttingType, boolean natural, short transparency) {
        super(name, origin, value, color, cuttingType);
        this.natural = natural;
        this.transparency = transparency;
    }

    public PreciousGem() {

    }

    public short getTransparency() {
        return transparency;
    }

    public boolean isNatural() {
        return natural;
    }

    public void setNatural(boolean natural) {
        this.natural = natural;
    }

    public void setTransparency(short transparency) {
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return super.toString() + "PreciousGem{" +
                "natural=" + natural +
                ", transparency=" + transparency +
                '}';
    }
}
