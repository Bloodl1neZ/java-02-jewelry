package com.epam.xml.entity;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "gems", namespace = "http://www.example.com/gems")
public class Gems {
    @XmlElements({
            @XmlElement(name = "stone", type = Stone.class),
            @XmlElement(name = "precious-gem", type = PreciousGem.class),
            @XmlElement(name = "semi-precious-gem", type = SemiPreciousGem.class)
    })
    private List<Stone> stoneList;

    public List<Stone> getStoneList() {
        return stoneList;
    }

    public void setStoneList(List<Stone> stoneList) {
        this.stoneList = stoneList;
    }
}
