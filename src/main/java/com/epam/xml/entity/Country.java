package com.epam.xml.entity;

public enum Country {
    RUSSIA,
    CANADA,
    AUSTRALIA,
    USA,
    BRAZIL
}
