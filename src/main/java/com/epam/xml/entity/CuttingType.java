package com.epam.xml.entity;

public enum CuttingType {
    SQUARE,
    ROUND,
    ROSE,
    OCTAGON,
    PEAR
}
