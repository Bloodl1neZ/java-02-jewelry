package com.epam.xml.entity;

import com.epam.xml.entity.adapter.ColorAdapter;
import com.epam.xml.entity.adapter.CountryAdapter;
import com.epam.xml.entity.adapter.CuttingTypeAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class Stone {
    @XmlAttribute(name = "name")
    private String name;

    @XmlJavaTypeAdapter(CountryAdapter.class)
    @XmlElement(name = "origin")
    private Country origin;

    @XmlElement(name = "value")
    private BigDecimal value;

    @XmlJavaTypeAdapter(ColorAdapter.class)
    @XmlElement(name = "color")
    private Color color;

    @XmlJavaTypeAdapter(CuttingTypeAdapter.class)
    @XmlElement(name = "cutting-type")
    private CuttingType cuttingType;

    public Stone(String name, Country origin, BigDecimal value, Color color, CuttingType cuttingType) {
        this.name = name;
        this.origin = origin;
        this.value = value;
        this.color = color;
        this.cuttingType = cuttingType;
    }

    public Stone() {

    }

    public String getName() {
        return name;
    }

    public Country getOrigin() {
        return origin;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Color getColor() {
        return color;
    }

    public CuttingType getCuttingType() {
        return cuttingType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrigin(Country origin) {
        this.origin = origin;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setCuttingType(CuttingType cuttingType) {
        this.cuttingType = cuttingType;
    }

    @Override
    public String toString() {
        return "Stone{" +
                "name='" + name + '\'' +
                ", origin=" + origin +
                ", value=" + value +
                ", color=" + color +
                ", cuttingType=" + cuttingType +
                '}';
    }
}
