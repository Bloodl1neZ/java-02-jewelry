package com.epam.xml.entity.adapter;

import com.epam.xml.entity.CuttingType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CuttingTypeAdapter extends XmlAdapter<String, CuttingType> {
    @Override
    public CuttingType unmarshal(String cuttingType) {
        String cuttingTypeUpper = cuttingType.toUpperCase();
        return CuttingType.valueOf(cuttingTypeUpper);
    }

    @Override
    public String marshal(CuttingType cuttingType) {
        return cuttingType.toString();
    }
}
