package com.epam.xml.entity.adapter;

import com.epam.xml.entity.Source;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SourceAdapter extends XmlAdapter<String, Source> {
    @Override
    public Source unmarshal(String source) {
        String sourceUpper = source.toUpperCase();
        return Source.valueOf(sourceUpper);
    }

    @Override
    public String marshal(Source source) {
        return source.toString();
    }
}
