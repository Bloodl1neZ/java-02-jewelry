package com.epam.xml.entity.adapter;

import com.epam.xml.entity.Country;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CountryAdapter extends XmlAdapter<String, Country> {
    @Override
    public Country unmarshal(String country) {
        String countryUpper = country.toUpperCase();
        return Country.valueOf(countryUpper);
    }

    @Override
    public String marshal(Country country) {
        return country.toString();
    }

}
