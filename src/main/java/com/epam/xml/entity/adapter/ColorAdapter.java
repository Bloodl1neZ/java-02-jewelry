package com.epam.xml.entity.adapter;

import com.epam.xml.entity.Color;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ColorAdapter extends XmlAdapter<String, Color> {
    @Override
    public Color unmarshal(String color) {
        String colorUpper = color.toUpperCase();
        return Color.valueOf(colorUpper);
    }

    @Override
    public String marshal(Color color) {
        return color.toString();
    }
}
