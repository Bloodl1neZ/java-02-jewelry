package com.epam.xml.entity;

public enum Color {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    PURPLE,
    BLACK
}
