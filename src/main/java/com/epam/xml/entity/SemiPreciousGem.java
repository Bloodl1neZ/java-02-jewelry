package com.epam.xml.entity;

import com.epam.xml.entity.adapter.SourceAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@XmlSeeAlso({Stone.class})
@XmlRootElement(name = "semi-precious-gem")
public class SemiPreciousGem extends Stone {

    @XmlJavaTypeAdapter(SourceAdapter.class)
    @XmlElement(name = "source")
    private Source source;

    public SemiPreciousGem(String name, Country origin, BigDecimal value, Color color,
                           CuttingType cuttingType, Source source) {
        super(name, origin, value, color, cuttingType);
        this.source = source;
    }

    public SemiPreciousGem() {

    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return super.toString() + "SemiPreciousGem{" +
                "source=" + source +
                '}';
    }
}
