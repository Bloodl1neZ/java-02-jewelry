package com.epam.xml.entity;

public enum Source {
    MAGMA,
    METAMORPHIC,
    ORGANIC
}
