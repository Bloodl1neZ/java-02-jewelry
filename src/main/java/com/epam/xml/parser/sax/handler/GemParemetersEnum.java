package com.epam.xml.parser.sax.handler;

public enum GemParemetersEnum {
    GEMS,
    ORIGIN,
    VALUE,
    COLOR,
    CUTTING_TYPE,
    NATURAL,
    TRANSPARENCY,
    SOURCE
}
