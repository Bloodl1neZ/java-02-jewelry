package com.epam.xml.parser.sax.handler;

import com.epam.xml.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class GemHandler extends DefaultHandler {
    private List<Stone> stoneList;
    private Stone stone;
    private GemParemetersEnum currentTag;
    private EnumSet<GemParemetersEnum> tagSet;

    public GemHandler() {
        stoneList = new ArrayList<>();
        tagSet = EnumSet.range(GemParemetersEnum.ORIGIN, GemParemetersEnum.SOURCE);
    }

    public List<Stone> getStoneList() {
        return stoneList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName) {
            case "precious-gem":
                stone = new PreciousGem();
                String preciousGemName = attributes.getValue("name");
                stone.setName(preciousGemName);
                break;
            case "semi-precious-gem":
                stone = new SemiPreciousGem();
                String semiPreciousGemName = attributes.getValue("name");
                stone.setName(semiPreciousGemName);
                break;
                default:
                    String qNameUpperCase = qName.toUpperCase();
                    String qNameUpperEnumStyle = qNameUpperCase.replaceAll("-","_");
                    GemParemetersEnum temp = GemParemetersEnum.valueOf(qNameUpperEnumStyle);
                    if(tagSet.contains(temp)) {
                        currentTag = temp;
                    }
                    break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if(qName.equalsIgnoreCase("precious-gem")
                || qName.equalsIgnoreCase("semi-precious-gem")) {
            stoneList.add(stone);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String str = new String(ch, start, length).trim();
        String upperCaseStr = str.toUpperCase();
            if(currentTag == null) {
                return;
            }
            switch (currentTag) {
                case COLOR:
                    Color color = Color.valueOf(upperCaseStr);
                    stone.setColor(color);
                    break;
                case VALUE:
                    BigDecimal value = new BigDecimal(str);
                    stone.setValue(value);
                    break;
                case ORIGIN:
                    Country country = Country.valueOf(upperCaseStr);
                    stone.setOrigin(country);
                    break;
                case CUTTING_TYPE:
                    CuttingType cuttingType = CuttingType.valueOf(upperCaseStr);
                    stone.setCuttingType(cuttingType);
                    break;
                case SOURCE:
                    Source source = Source.valueOf(upperCaseStr);
                    ((SemiPreciousGem)stone).setSource(source);
                    break;
                case NATURAL:
                    boolean natural = Boolean.parseBoolean(str);
                    ((PreciousGem)stone).setNatural(natural);
                    break;
                case TRANSPARENCY:
                    short transparency = Short.parseShort(str);
                    ((PreciousGem)stone).setTransparency(transparency);
                    break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentTag.getDeclaringClass(), currentTag.name());
            }
        currentTag = null;
    }


}
