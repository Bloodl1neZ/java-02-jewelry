package com.epam.xml.parser.sax;

import com.epam.xml.entity.Stone;
import com.epam.xml.exception.ParserException;
import com.epam.xml.parser.Parser;
import com.epam.xml.parser.sax.handler.GemHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;
import org.xml.sax.SAXException;

public class SaxParser implements Parser {
    private  GemHandler gemHandler;
    private SAXParser saxParser;
    private SAXParserFactory saxParserFactory;

    public SaxParser() {
        gemHandler = new GemHandler();
        saxParserFactory = SAXParserFactory.newInstance();
    }

    public List<Stone> parse(String path) throws ParserException {
        try{
            saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(path, gemHandler);
        } catch (ParserConfigurationException | IOException | SAXException ex) {
            throw new ParserException(ex.getMessage(), ex);
        }
        return gemHandler.getStoneList();
    }

}
