package com.epam.xml.parser;

import com.epam.xml.parser.dom.DomParser;
import com.epam.xml.parser.jaxb.JaxbParser;
import com.epam.xml.parser.sax.SaxParser;

public class ParserFactory {
    public Parser createParser(TypeParser typeParser) {
        switch (typeParser) {
            case DOM:
                return new DomParser();
            case JAXB:
                return new JaxbParser();
            case SAX:
                return new SaxParser();
            default:
                throw new IllegalArgumentException("Illegal Parser Type");
        }
    }
}
