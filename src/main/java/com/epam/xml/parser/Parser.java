package com.epam.xml.parser;

import com.epam.xml.entity.Stone;
import com.epam.xml.exception.ParserException;

import java.util.List;

public interface Parser {
    List<Stone> parse(String path) throws ParserException;
}
