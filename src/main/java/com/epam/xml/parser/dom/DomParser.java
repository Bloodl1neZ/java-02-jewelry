package com.epam.xml.parser.dom;

import com.epam.xml.entity.*;
import com.epam.xml.exception.ParserException;
import com.epam.xml.parser.Parser;
import com.epam.xml.parser.dom.builder.GemBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomParser implements Parser {
    private DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    GemBuilder gemBuilder = new GemBuilder();

    public List<Stone> parse(String path) throws ParserException {
        List<Stone> stoneList = new ArrayList<Stone>();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);
            Element root = document.getDocumentElement();
            addPreciousGemsToList(root, stoneList);
            addSemiPreciousGemsToList(root, stoneList);
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            throw new ParserException(ex.getMessage(), ex);
        }
        return stoneList;
    }

    private void addPreciousGemsToList(Element root, List<Stone> stoneList) {
        NodeList preciousGemList = root.getElementsByTagName("precious-gem");
        for(int i = 0; i < preciousGemList.getLength(); ++i) {
            Element preciousGemElement = (Element) preciousGemList.item(i);
            Stone preciousGem = gemBuilder.buildPreciousGem(preciousGemElement);
            stoneList.add(preciousGem);
        }
    }

    private void addSemiPreciousGemsToList(Element root, List<Stone> stoneList) {
        NodeList semiPreciousGemList = root.getElementsByTagName("semi-precious-gem");
        for(int i = 0; i < semiPreciousGemList.getLength(); ++i) {
            Element semiPreciousGemElement = (Element) semiPreciousGemList.item(i);
            Stone semiPreciousGem = gemBuilder.buildSemiPreciousGem(semiPreciousGemElement);
            stoneList.add(semiPreciousGem);
        }
    }
}
