package com.epam.xml.parser.dom.builder;

import com.epam.xml.entity.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;

public class GemBuilder {
    public PreciousGem buildPreciousGem(Element element) {
        PreciousGem preciousGem = new PreciousGem();

        String transparencyStr = getElementTextContent(element, "transparency");
        short transparency = Short.parseShort(transparencyStr);
        preciousGem.setTransparency(transparency);

        String naturalStr = getElementTextContent(element, "natural");
        boolean natural = Boolean.parseBoolean(naturalStr);
        preciousGem.setNatural(natural);

        return (PreciousGem) buildStonePart(element, preciousGem);
    }

    public SemiPreciousGem buildSemiPreciousGem(Element element) {
        SemiPreciousGem semiPreciousGem = new SemiPreciousGem();

        String sourceStr = getElementTextContent(element, "source");
        String sourceUpper = sourceStr.toUpperCase();
        Source source = Source.valueOf(sourceUpper);
        semiPreciousGem.setSource(source);

        return (SemiPreciousGem) buildStonePart(element, semiPreciousGem);
    }

    private Stone buildStonePart(Element element, Stone stone) {
        String name = element.getAttribute("name");
        stone.setName(name);

        String originStr = getElementTextContent(element, "origin");
        String originUpper = originStr.toUpperCase();
        Country origin = Country.valueOf(originUpper);
        stone.setOrigin(origin);

        String valueStr = getElementTextContent(element, "value");
        BigDecimal value = new BigDecimal(valueStr);
        stone.setValue(value);

        String gemColorStr = getElementTextContent(element, "color");
        String colorUpper = gemColorStr.toUpperCase();
        Color color = Color.valueOf(colorUpper);
        stone.setColor(color);

        String cuttingTypeStr = getElementTextContent(element, "cutting-type");
        String cuttingTypeUpper = cuttingTypeStr.toUpperCase();
        CuttingType cuttingType = CuttingType.valueOf(cuttingTypeUpper);
        stone.setCuttingType(cuttingType);

        return stone;
    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        String textContent = node.getTextContent();
        return textContent;
    }
}
