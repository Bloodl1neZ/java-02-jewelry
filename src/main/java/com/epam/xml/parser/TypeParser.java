package com.epam.xml.parser;

public enum TypeParser {
    DOM,
    JAXB,
    SAX
}
