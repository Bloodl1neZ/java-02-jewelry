package com.epam.xml.parser.jaxb;

import com.epam.xml.entity.Stone;
import com.epam.xml.entity.Gems;
import com.epam.xml.exception.ParserException;
import com.epam.xml.parser.Parser;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class JaxbParser implements Parser {

    public List<Stone> parse(String path) throws ParserException {
        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(Gems.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Gems gems = (Gems) jaxbUnmarshaller.unmarshal(file);
            return gems.getStoneList();
        } catch(JAXBException ex) {
            throw new ParserException(path, ex);
        }
    }
}
